#include "Camera.h"
#include <glm\gtx\transform.hpp>

const float SPEED = 0.001f;

Camera::Camera() : 
	viewDirection(0.0f, 0.0f, -1.0),
	UP(0.0f, 1.0f, 0.0f)
{
	initialMove = true;

}

void Camera::mouseUpdate(const vec2& newMousePos) {
	vec2 mouseDelta = newMousePos - oldMousePos;
	if (initialMove) {
		oldMousePos = newMousePos;
		initialMove = false;
	}
	if (length(mouseDelta) > 50.0f) {
		return;
	}

	float rotSpeed = 0.3f;
	viewDirection = mat3(rotate(-mouseDelta.x * rotSpeed, UP)) * viewDirection;

	vec3 verticalRotateAround = cross(viewDirection, UP);
	float final_y = (oldMousePos.y + mouseDelta.y)*rotSpeed;

	if (final_y > 89.f)
		mouseDelta.y = 0.f;
	if (final_y < -89.f)
		mouseDelta.y = 0.f;
	
	viewDirection = mat3(rotate(-mouseDelta.y * rotSpeed, verticalRotateAround)) * viewDirection;

	oldMousePos = newMousePos;
}

mat4 Camera::getViewMatrix() const {
	return lookAt(position, position + viewDirection, UP);
}

void Camera::moveForwards() {
	position += SPEED * viewDirection;
}

void Camera::moveBackwards() {
	position += -SPEED * viewDirection;
}

void Camera::strafeLeft() {
	vec3 strafeDir = cross(viewDirection, UP);
	position += -SPEED * strafeDir;
}
void Camera::strafeRight() {
	vec3 strafeDir = cross(viewDirection, UP);
	position += SPEED * strafeDir;

}
void Camera::moveUp() {
	position += SPEED * UP;
}
void Camera::moveDown() {
	position += -SPEED * UP;
}

void Camera::positionUpdate() {

	if (key_state[D])
		strafeRight();
	if (key_state[A])
		strafeLeft();
	if (key_state[S])
		moveBackwards();
	if (key_state[W])
		moveForwards();
	if (key_state[SPACE])
		moveUp();
	if (key_state[F])
		moveDown();

}