#pragma once
#include <glm\glm.hpp>

using namespace glm;

class Camera
{
	vec3 viewDirection;
	const vec3 UP;
	vec2 oldMousePos;
	bool initialMove;

public:
	// MOVEMENT KEYS (WASD):
	bool key_state[6] = { false, false, false, false, false, false }; // true for pressed, false for released
	enum keys { W, A, S, D, SPACE, F }; // w=0, a=1, s=2, d=3, space=4, F=5
	vec3 position;

	Camera();
	void mouseUpdate(const vec2& newMousePos);
	mat4 getViewMatrix() const;

	void moveForwards();
	void moveBackwards();
	void strafeLeft();
	void strafeRight();
	void moveUp();
	void moveDown();
	void positionUpdate();

};