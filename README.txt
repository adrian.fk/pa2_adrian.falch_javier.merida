 ____________________________________________________
|					
|   3D Solar system
|					
|	Author: Adrian Falch Karlsen
|		Javier Merida
|____________________________________________________
|
|	Camera controls:		
|
|	Move the camera with the mouse	
|	meanwhile the left mouse button
|	is pressed.		
|				
|	Move forwards:	W	
|	Move backwards:	S	
|	Strafe right:	D	
|	Strafe left:	A	
|	Move upwards:	SPACE	
|	Move downwards:	F	
|	Stop earth rotation: R		
|
|____________________________________________________
|
|	Added fuctionality:
|	   
|	   Enabled for moving both up and down.
|	   
|	   Added button to stop rotation of the earth
|	   alltogether.
|	
|____________________________________________________