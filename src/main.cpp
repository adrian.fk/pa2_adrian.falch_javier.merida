//include some standard libraries
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <string>

//include OpenGL libraries
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


//include some custom code files
#include "glfunctions.h" //include all OpenGL stuff
#include "Shader.h" // class to compile shaders
#include "../visual_studio/Camera.h"

#include <string>
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include "imageloader.h"

#define AMT_PLANETS 10
#define SUN 0
#define MERCURY 1
#define VENUS 2
#define EARTH 3
#define MARS 4
#define JUPITER 5
#define SATURN 6
#define URANUS 7
#define NEPTUNE 8
#define PLUTO 9

using namespace std;
using namespace glm;
using glm::mat4;

string basepath = "assets/";
string inputfile_sphere = basepath + "sphere.obj";
vector< tinyobj::shape_t > shapes;    // the vector that will hold the mesh data
vector< tinyobj::shape_t > skyboxShapes;
vector< tinyobj::material_t > materials;
vector< tinyobj::material_t > skyboxMaterials;


//global variables to help us do things
int g_ViewportWidth = 800; int g_ViewportHeight = 800; // Default window size, in pixels
double mouse_x, mouse_y; //variables storing mouse position
const vec3 g_backgroundColor(0.0f, 0.0f, 0.0f); // background colour - a GLM 3-component vector

GLfloat translateX = 0.0f;
GLfloat translateY = 0.0f;

Camera camera;

float planet_spin_rate[AMT_PLANETS-1-SUN];
float planet_spin[AMT_PLANETS - 1 - SUN]; 
bool lock_spin = false;

#define AMT_TEXTURES 20
GLuint texture_id[AMT_TEXTURES];
GLuint sky_texture_id = 0;

GLuint g_simpleShader = 0; //shader identifier
GLuint g_simpleShader_sky = 0;
GLuint planets_Vaos[AMT_PLANETS]; //vaos
GLuint g_Vao_sky = 0;
GLuint planets_numTriangles[AMT_PLANETS]; //  Number count of triangles we are painting.
GLuint g_NumTriangles_sky = 0;

vec3 g_light_dir(10.0, 0.0, 10.0f);

//Planet size factors
float planetSizeFactor[AMT_PLANETS];


int mouseLeftPressed;


void load_skyBox() 
{
	string err;
	bool  ret;

	//Loading SkyBox
	ret = tinyobj::LoadObj(skyboxShapes, skyboxMaterials, err, inputfile_sphere.c_str(), basepath.c_str());

	//test if OBJ loaded correctly
	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
		exit(1);
	}
	else
		//print out number of meshes described in file
		std::cout << "Loaded Skybox w. " << inputfile_sphere << " with shapes: " << shapes.size() << std::endl;

	Shader simpleShader_sky("src/shader_sky.vert", "src/shader_sky.frag");
	g_simpleShader_sky = simpleShader_sky.program;

	g_Vao_sky = gl_createAndBindVAO();
	gl_createAndBindAttribute(&(skyboxShapes[0].mesh.positions[0]),
								skyboxShapes[0].mesh.positions.size() * sizeof(float),
								g_simpleShader_sky,
								"a_vertex",
								3);

	gl_createIndexBuffer(&(skyboxShapes[0].mesh.indices[0]),
							skyboxShapes[0].mesh.indices.size() * sizeof(unsigned int));

	gl_createAndBindAttribute(&(skyboxShapes[0].mesh.texcoords[0]), 
								skyboxShapes[0].mesh.texcoords.size() * sizeof(GLfloat),
								g_simpleShader_sky,
								"a_uv",
								2);

	gl_unbindVAO();
	g_NumTriangles_sky = skyboxShapes[0].mesh.indices.size() / 3;

	Image* image = loadBMP("assets/milkyway.bmp");


	glGenTextures(1, &(sky_texture_id));
	glBindTexture(GL_TEXTURE_2D, sky_texture_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_RGB,
		image->width,
		image->height,
		0,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		image->pixels);

}


// ------------------------------------------------------------------------------------------
// This function manually creates a square geometry (defined in the array vertices[])
// ------------------------------------------------------------------------------------------
void load()
{
	string err;
	bool  ret;

	//Planet size multiplicity factors
	planetSizeFactor[SUN] = 0.60f;
	planetSizeFactor[MERCURY] = 0.05f;
	planetSizeFactor[VENUS] = 0.06f;
	planetSizeFactor[EARTH] = 0.07f;
	planetSizeFactor[MARS] = 0.07f;
	planetSizeFactor[JUPITER] = 0.30f;
	planetSizeFactor[SATURN] = 0.20f;
	planetSizeFactor[URANUS] = 0.10f;
	planetSizeFactor[NEPTUNE] = 0.09f;
	planetSizeFactor[PLUTO] = 0.02f;
	
	//Planet spin factors
	planet_spin_rate[MERCURY - 1] = 0.5f;
	planet_spin_rate[VENUS - 1] = 1.6f;
	planet_spin_rate[EARTH - 1] = 0.20f;
	planet_spin_rate[MARS - 1] = 3.07f;
	planet_spin_rate[JUPITER - 1] = 1.30f;
	planet_spin_rate[SATURN - 1] = 1.20f;
	planet_spin_rate[URANUS - 1] = 1.10f;
	planet_spin_rate[NEPTUNE - 1] = 3.09f;
	planet_spin_rate[PLUTO - 1] = 5.02f;
	
	load_skyBox();

	//load the shader
	Shader simpleShader("src/shader.vert", "src/shader.frag");
	g_simpleShader = simpleShader.program;

	//load sphere OBJ file to shapes
	ret = tinyobj::LoadObj(shapes, materials, err, inputfile_sphere.c_str(), basepath.c_str());

	//we assign a colour to each vertex (all of the vertices as assigned WHITE)
	vector<GLfloat> colors(shapes[0].mesh.positions.size(), 1.0);

	//test if OBJ loaded correctly
	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
		exit(1);
	}
	else
		//print out number of meshes described in file
		std::cout << "Loaded " << inputfile_sphere << " with shapes: " << shapes.size() << std::endl;

	//Load planet textures
	for (int i = 0; i < AMT_PLANETS; i++) {
		// Create the VAO
		planets_Vaos[i] = gl_createAndBindVAO();

		// Bind the vectors/arrays to the attributes in the shaders 
		gl_createAndBindAttribute(&(shapes[0].mesh.positions[0]), shapes[0].mesh.positions.size() * sizeof(float), g_simpleShader, "a_vertex", 3);
		gl_createAndBindAttribute(&(colors[0]), colors.size() * sizeof(GLfloat), g_simpleShader, "a_color", 3);
		gl_createIndexBuffer(&(shapes[0].mesh.indices[0]), shapes[0].mesh.indices.size() * sizeof(unsigned int));

		gl_createAndBindAttribute(
			&(shapes[0].mesh.texcoords[0]),
			shapes[0].mesh.texcoords.size() * sizeof(GLfloat),
			g_simpleShader,
			"a_uv", 2);

		gl_createAndBindAttribute(&(shapes[0].mesh.normals[0]),
			shapes[0].mesh.normals.size() * sizeof(float), g_simpleShader, "a_normal", 3);

		planets_numTriangles[i] = shapes[0].mesh.indices.size() / 3;

		gl_unbindVAO();

		string planetNum = to_string(i);
		string planetPath = "assets/planet" + planetNum + ".bmp";
		Image* image = loadBMP(planetPath.c_str());


		glGenTextures(1, &(texture_id[i]));
		glBindTexture(GL_TEXTURE_2D, texture_id[i]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			image->width,
			image->height,
			0,
			GL_RGB,
			GL_UNSIGNED_BYTE,
			image->pixels);
	}
}

// ------------------------------------------------------------------------------------------
// This function actually draws to screen and called non-stop, in a loop
// ------------------------------------------------------------------------------------------
void draw()
{	
	//clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glDisable(GL_DEPTH_TEST);
	//Activate Skybox shader
	glUseProgram(g_simpleShader_sky);
	//Binding Skybox VAO
	gl_bindVAO(g_Vao_sky);

	mat4 transformation_matrix;
	mat4 sphereModelMatrix;

	//World to view matrix
	mat4 view_matrix = camera.getViewMatrix();

	GLuint u_model = glGetUniformLocation(g_simpleShader_sky, "u_model");
	GLuint u_view = glGetUniformLocation(g_simpleShader_sky, "u_view");
	GLuint u_projection = glGetUniformLocation(g_simpleShader_sky, "u_projection");

	//setting M & P for Sky Box
	mat4 model_matrix = translate(scale(mat4(1.0f), vec3(12, 12, 12)), vec3(0.0, 0.0, 0.0));
	mat4 projection_matrix = perspective(120.0f, (float)(g_ViewportWidth / g_ViewportHeight),
											0.1f, 50.0f);

	//Sending skybox to shader
	glUniformMatrix4fv(u_model, 1, GL_FALSE, value_ptr(model_matrix));
	glUniformMatrix4fv(u_view, 1, GL_FALSE, value_ptr(view_matrix));
	glUniformMatrix4fv(u_projection, 1, GL_FALSE, value_ptr(projection_matrix));

	GLuint u_texture_sky = glGetUniformLocation(g_simpleShader_sky, "u_texture");
	glUniform1i(u_texture_sky, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sky_texture_id);

	glDrawElements(GL_TRIANGLES, 3 * g_NumTriangles_sky, GL_UNSIGNED_INT, 0);

	gl_unbindVAO();

	// activate shader
	glUseProgram(g_simpleShader);

	GLuint colorLoc = glGetUniformLocation( g_simpleShader, "u_color");
	
	GLuint u_texture = glGetUniformLocation(g_simpleShader, "u_texture");
	
	// link the matrix to the u_model shader uniform variable in the shader
	GLuint model_loc = glGetUniformLocation(g_simpleShader, "u_model");

	// link the normal matrix to the m_normal shader uniform variable in the shader
	GLuint u_normal = glGetUniformLocation(g_simpleShader, "u_normal");


	//create projection matrix and pass to shader
	projection_matrix = perspective(
		60.0f, // Field of view
		1.0f, // Aspect ratio
		0.1f, // near plane (distance from camera)
		10.0f // Far plane (distance from camera)
	);
	GLuint projection_loc = glGetUniformLocation(g_simpleShader, "u_projection");
	glUniformMatrix4fv(projection_loc, 1, GL_FALSE,
		glm::value_ptr(projection_matrix));


	//SPAWN PLANETS
	for (int targeted_planet = AMT_PLANETS - 1; targeted_planet >= 0; --targeted_planet) {
		glCullFace(GL_BACK);
		glEnable(GL_DEPTH_TEST);

		float targeted_planet_f = (float)targeted_planet;
		glUniform3f(colorLoc, 1.0f, 0.0f, 0.0f);

		glUniform1i(u_texture, 0);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_id[targeted_planet]);

		//bind the geometry
		gl_bindVAO(planets_Vaos[targeted_planet]);
		glBindVertexArray(planets_Vaos[targeted_planet]);

		sphereModelMatrix = translate(mat4(1.0f), vec3(-targeted_planet_f + (float)EARTH, 0.0f, -3.0f));  //+ (float) EARTH to put earth in the middle of spawning scope
		mat4 normal_matrix;
		if (targeted_planet != SUN) {
			normal_matrix = rotate(mat4(1.0f), radians(planet_spin[targeted_planet - 1]), vec3(0.0f, 1.0f, 0.0f));
			sphereModelMatrix = sphereModelMatrix * normal_matrix;
			//normal_matrix = sphereModelMatrix;
		}
		sphereModelMatrix = scale(sphereModelMatrix, vec3(planetSizeFactor[targeted_planet], 
															planetSizeFactor[targeted_planet], 
															planetSizeFactor[targeted_planet]));

		transformation_matrix = view_matrix * sphereModelMatrix;

		planet_spin[targeted_planet-1] += planet_spin_rate[targeted_planet-1] * lock_spin?0.f: 1.f;

		// pass the data to the shader
		glUniformMatrix4fv(model_loc,
			1,
			GL_FALSE,
			glm::value_ptr(transformation_matrix)
		);

		glUniformMatrix4fv(u_normal,
			1,
			GL_FALSE,
			glm::value_ptr(normal_matrix)
		);

		GLuint light_loc = glGetUniformLocation(g_simpleShader, "u_light_dir");
		vec3 sunPos = vec3((float)EARTH, 0.0f, 0.0f);
		glUniform3f(light_loc, sunPos.x, sunPos.y, sunPos.z);

		// Phong light settings
		if (targeted_planet == SUN) {
			GLuint ambient_loc = glGetUniformLocation(g_simpleShader, "u_ambient");
			glUniform3f(ambient_loc, 1, 1.f, 1.f);

			GLuint diffuse_loc = glGetUniformLocation(g_simpleShader, "u_diffuse");
			glUniform3f(diffuse_loc, 0.0, 0.0, 0.0);

			GLuint specular_loc = glGetUniformLocation(g_simpleShader, "u_specular");
			glUniform3f(specular_loc, 1.0, 0.0, 0.0);

			GLuint cam_pos_loc = glGetUniformLocation(g_simpleShader, "u_cam_pos");
			glUniform3f(cam_pos_loc, 0.0f, 0.0f, 0.0f);

			GLuint shininess_loc = glGetUniformLocation(g_simpleShader, "u_shininess");
			glUniform1f(shininess_loc, 40.0);
		} else {
			GLuint ambient_loc = glGetUniformLocation(g_simpleShader, "u_ambient");
			glUniform3f(ambient_loc, 0.4f, 0.4f, 0.4f);

			GLuint diffuse_loc = glGetUniformLocation(g_simpleShader, "u_diffuse");
			glUniform3f(diffuse_loc, 1.0f, 1.0f, 1.0f);

			GLuint specular_loc = glGetUniformLocation(g_simpleShader, "u_specular");
			glUniform3f(specular_loc, 2.f, 2.f, 1.f);

			GLuint cam_pos_loc = glGetUniformLocation(g_simpleShader, "u_cam_pos");
			glUniform3f(cam_pos_loc, camera.position.x, camera.position.y, camera.position.z);

			GLuint shininess_loc = glGetUniformLocation(g_simpleShader, "u_shininess");
			glUniform1f(shininess_loc, 40.0);
		}

		// Draw to screen
		glDrawElements(GL_TRIANGLES, 3 * planets_numTriangles[targeted_planet], GL_UNSIGNED_INT, 0);
		gl_unbindVAO();
	}
}

// ------------------------------------------------------------------------------------------
// This function is called every time you press a screen
// ------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    //quit
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, 1);
	//reload
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		load();

	if (key == GLFW_KEY_D && action ==  GLFW_PRESS)
		camera.key_state[camera.D] = true;
	if (key == GLFW_KEY_D && action == GLFW_RELEASE)
		camera.key_state[camera.D] = false;

	if (key == GLFW_KEY_A && action == GLFW_PRESS)
		camera.key_state[camera.A] = true;
	if (key == GLFW_KEY_A && action == GLFW_RELEASE)
		camera.key_state[camera.A] = false;

	if (key == GLFW_KEY_W && action == GLFW_PRESS)
		camera.key_state[camera.W] = true;
	if (key == GLFW_KEY_W && action == GLFW_RELEASE)
		camera.key_state[camera.W] = false;

	if (key == GLFW_KEY_S && action == GLFW_PRESS)
		camera.key_state[camera.S] = true;
	if (key == GLFW_KEY_S && action == GLFW_RELEASE)
		camera.key_state[camera.S] = false;

	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		camera.key_state[camera.SPACE] = true;
	if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
		camera.key_state[camera.SPACE] = false;

	if (key == GLFW_KEY_F && action == GLFW_PRESS)
		camera.key_state[camera.F] = true;
	if (key == GLFW_KEY_F && action == GLFW_RELEASE)
		camera.key_state[camera.F] = false;

	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		lock_spin = !lock_spin;

	cout << "X: " << camera.position.x << ", Y: " << camera.position.y << ", Z: " << camera.position.z << endl;
	draw();
}

// ------------------------------------------------------------------------------------------
// This function is called every time you click the mouse
// ------------------------------------------------------------------------------------------
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		mouseLeftPressed = 1;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
		mouseLeftPressed = 0;
	}
}

// This function is called when the mouse moves
void onMouseMove(GLFWwindow* window, double x, double y) {
	if (mouseLeftPressed) {
		camera.mouseUpdate(vec2(x, y));
		draw();
	}
}

int main(void)
{
	//printing player controls
	cout << "Controls:" << endl;
	cout << "Move camera with the mouse" << endl;
	cout << "Move forwards: W" << endl;
	cout << "Move backwards: S" << endl;
	cout << "Strafe right: D" << endl;
	cout << "Strafe left: A" << endl;
	cout << "Move upwards: SPACE" << endl;
	cout << "Move downwards: F" << endl;


	//setup window and boring stuff, defined in glfunctions.cpp
	GLFWwindow* window;
	if (!glfwInit())return -1;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(g_ViewportWidth, g_ViewportHeight, "Hello OpenGL!", NULL, NULL);
	if (!window) {glfwTerminate();	return -1;}
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;
	glewInit();
	
	//glEnable(GL_DEPTH_TEST);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//input callbacks
	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);
	glfwSetCursorPosCallback(window, onMouseMove);


	//load all the resources
	load();

    // Loop until the user closes the window
    while (!glfwWindowShouldClose(window))
    {
		camera.positionUpdate();

		draw();

        // Swap front and back buffers
        glfwSwapBuffers(window);
        
        // Poll for and process events
        glfwPollEvents();
        
        //mouse position must be tracked constantly (callbacks do not give accurate delta)
        glfwGetCursorPos(window, &mouse_x, &mouse_y);
    }

    //terminate glfw and exit
    glfwTerminate();
    return 0;
}


