#version 330
 
in vec3 a_vertex;
in vec3 a_color;
in vec2 a_uv;
in vec3 a_normal;

uniform mat4 u_normal;
uniform mat4 u_model;
uniform mat4 u_projection;

out vec2 v_uv;
out vec3 v_color;

out vec3 v_normal;
out vec3 v_vertex;

void main()
{
	// pass the colour to the fragment shader
	v_color = a_color;

	// pass the uv coordinates to the fragment shader
	v_uv = a_uv;
	v_vertex = (u_model * vec4( a_vertex , 1.0 )).xyz;
	v_normal = (u_normal * vec4( a_normal, 1.0 )).xyz;

	// position of the vertex
	gl_Position = u_projection * u_model * vec4( a_vertex , 1.0 );

}

