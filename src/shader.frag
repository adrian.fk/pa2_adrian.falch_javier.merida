#version 330

uniform vec3 u_color;
uniform sampler2D u_texture;

out vec4 fragColor;

in vec2 v_uv;

in vec3 v_vertex;
in vec3 v_normal;

uniform vec3 u_light_dir;
uniform vec3 u_ambient;
uniform vec3 u_diffuse;
uniform vec3 u_specular;
uniform vec3 u_cam_pos;

uniform float u_shininess;

void main(void)
{
	vec3 texture_color = texture(u_texture, v_uv).xyz;
	
	vec3 N = normalize(v_normal);
	vec3 L = normalize(u_light_dir - v_vertex);
	float NdotL = max( dot(L,N) , 0.0f); 
 
	vec3 R = normalize(-reflect(L,N)); 
	vec3 E = normalize(u_cam_pos - v_vertex);	
	float RdotE = pow( max(dot(R,E), 0.0f), u_shininess);

	vec3 amb = texture_color * u_ambient;
	vec3 diff = texture_color * NdotL * u_diffuse;
	vec3 spec;

	if( dot(L,N) < 0.0){  // light source on the wrong side?
		spec = vec3(0.0, 0.0, 0.0);	// no specular reflection
	}else{
		spec = texture_color * u_specular * RdotE;  // specular :D
	}
	vec3 final_color = amb + diff + spec;
	fragColor =  vec4(final_color,1.0);     
	
}
