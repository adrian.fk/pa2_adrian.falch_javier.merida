#version 330

uniform sampler2D u_texture;

out vec4 fragColor;

in vec2 v_uv;


void main(void)
{
	vec4 texture_color = texture(u_texture, v_uv);
	fragColor =  vec4(texture_color.xyz, 1.0);
}
